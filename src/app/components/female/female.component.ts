import { Component, OnInit } from '@angular/core';
import { DataService } from 'src/app/services/data.service';
import { Chart } from 'angular-highcharts';
import { barChartOptions } from 'src/app/helpers/barChartOptions';

@Component({
  selector: 'app-female',
  templateUrl: './female.component.html',
  styleUrls: ['./female.component.css']
})

export class FemaleComponent implements OnInit {
  data: any;
  categories: string[] = [];
  barChart: Chart = new Chart();
  titleChart: string = 'Dades demogràfiques femenines x comarca a Catalunya';
  
  constructor(private dataService: DataService) { }

  async ngOnInit(): Promise<any> {
    this.data = this.dataService.getData();
    if (this.data == undefined) {                       // Check if data exists, (page reload)
      this.data = await this.dataService.getDataFromApi();
      this.data = this.dataService.getData();
    }
    this.dataService.setTitle(this.titleChart);
    this.dataService.setCategories(this.data);
    this.dataService.setValues(this.data, 1);
    this.barChart = new Chart(barChartOptions); 
  }   
}
