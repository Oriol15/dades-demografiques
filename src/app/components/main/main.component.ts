import { Component, OnInit } from '@angular/core';
import { HttpClient } from "@angular/common/http";
import { DataService } from 'src/app/services/data.service';

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.css']
})
export class MainComponent implements OnInit {
  data: any;
  constructor(private httpClient: HttpClient, private dataService: DataService) { }

  ngOnInit(): void {
    this.getData();
  }

  async getData(): Promise<any> {
    await this.dataService.getDataFromApi();
  }
}
