import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FemaleComponent } from './components/female/female.component';
import { MasculineComponent } from './components/masculine/masculine.component';
import { TotalsComponent } from './components/totals/totals.component';
import { MainComponent } from './components/main/main.component';

const routes: Routes = [
  { path: 'totals', component: TotalsComponent },
  { path: 'masculine', component: MasculineComponent },
  { path: 'female', component: FemaleComponent },
  { path: '**', component:  MainComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})

export class AppRoutingModule { }
