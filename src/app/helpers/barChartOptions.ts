import { Options } from 'highcharts';

export const barChartOptions: Options = {
  chart: {
    type: 'bar',
    height: 750,
    borderColor: '5px solid black'
  },
  credits: {
    enabled: false,
  },
  title: {
    text: '',
    margin: 50,
  },
  yAxis: {
    visible: true,
    gridLineColor: 'black',
  },
  legend: {
    enabled: false,
  },
  xAxis: {
    lineColor: 'black',
    labels: {
      style: {
          color: 'black'
      }
    },
    categories: [

    ],
  },

  plotOptions: {
    series: {
      borderRadius: 0,
    } as any,
  },

  series: [
    {
      name: 'Habitants:',
      type: 'bar',
      color: 'black',
      data: [

      ],
    },
  ],
};