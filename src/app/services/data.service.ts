import { Injectable } from '@angular/core';
import { HttpClient } from "@angular/common/http";
import { barChartOptions } from 'src/app/helpers/barChartOptions';

@Injectable({
  providedIn: 'root'
})
export class DataService {
  url: string = 'https://api.idescat.cat/pob/v1/geo.json?tipus=com&lang=es';
  data: any;
  constructor(private httpClient: HttpClient) { }

  async getDataFromApi(): Promise<any>{
    return this.httpClient.get<any>('https://api.idescat.cat/pob/v1/geo.json?tipus=com&lang=es', { responseType: 'json' }).toPromise()
    .then (response => {
        this.data = response;
        this.data = this.data['feed']['entry'];
    });
  }

  getData(){
    return this.data;
  }

  setCategories (data: any): void {
    let categories = [];
    for (let element of data){
      categories.push(element.title);
    }
    Object.values(barChartOptions)[5].categories = categories;
  }

  setTitle (title: string ):void{
    Object.values(barChartOptions)[2].text = title;
  }

  setValues (data: any, genre: any): void {
    let values = [];
    for (let element of data){
      values.push(parseInt(element['cross:DataSet']['cross:Section']['cross:Obs'][genre]['OBS_VALUE'])); 
    }
    Object.values(barChartOptions)[7][0].data = values;
  }
}
